const EntityService = require('../entity');
const Link = require('../../types/link');
const configuration = require('./link.service.config');

class LinkService extends EntityService {
    constructor(settings = {}) {
        super(Object.assign({}, configuration, settings));
    }

    generateKey(link) {
        return `${link.output_slot_id}-${link.output_slot_name}-${link.input_slot_id}-${link.input_slot_name}`;
    }
    
    fromKey(key) {
        const tokens = key.split('-');
        const output_owner_id = tokens[0];
        const output_slot_name = tokens[1];
        const input_owner_id = tokens[2];
        const input_slot_name = tokens[3];

        const AgentService = this.services.agent;

        const input_agent = AgentService.getById(input_owner_id);
        const input_slot = input_agent.getInputSlot(input_slot_name);
        
        const output_agent = AgentService.getById(output_owner_id);
        const output_slot = output_agent.getOutputSlot(output_slot_name);

        const link = new Link({
            _id: key,

            output_owner_id: output_owner_id,
            output_slot_id: output_slot._id,
            output_slot_name: output_slot_name,

            input_owner_id: input_owner_id,
            input_slot_id: input_slot._id,
            input_slot_name: input_slot_name
        });

        this.recordInstance(link);

        input_slot.onLink(link._id, output_slot);
        output_slot.onLink(link._id, input_slot);
    }

    create(settings = {}, another_slot = null) {
        const link = new Link(settings);
        
        const SlotService = this.services.slot;

        const input_slot = SlotService.getById(settings.input_slot_id);
        const output_slot = SlotService.getById(settings.output_slot_id);
    
        link.setId(this.generateKey(link));
        this.recordInstance(link);

        input_slot.onLink(link._id, output_slot);
        output_slot.onLink(link._id, input_slot);
        return link;
    }

    delete(link_id) {
        this.killById(link_id);
    }
}

module.exports = LinkService;
