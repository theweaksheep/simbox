module.exports = {
    name: `LinkService`,
    disableSkills: true,

    services: {
        slot: 'slot',
        agent: 'agent'
    }
};