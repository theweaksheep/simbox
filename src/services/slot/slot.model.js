const EntityService = require('../entity');
const Slot = require('../../types/slot');
const configuration = require('./slot.model.config');

class SlotService extends EntityService {
    constructor(settings = {}) {
        super(Object.assign({}, configuration, settings));
    }

    create(settings = {}) {
        const slot = new Slot(Object.assign({ app: this.app }, settings));
        this.recordInstance(slot);
        return slot;
    }

    delete(link) {
        
    }
}

module.exports = SlotService;
