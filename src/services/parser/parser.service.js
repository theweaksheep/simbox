const configuration = require('./parser.config');
const Entity = require('../../types/entity');

class ParserService extends Entity {
    constructor(settings = {}) {
        settings = Object.assign({}, JSON.parse(JSON.stringify(configuration)), settings);
        super(settings);
        this.container = {};
        this.initialize(settings);
        this.app = settings.app;
    }

    initialize(settings = {}) {
        configuration.ressources.forEach(ressource => {
            this.record(ressource.type, this.createParser(this.app.getRessource(ressource.name)));
        });

        if (Array.isArray(settings.ressources)) {
            settings.ressources.forEach(ressource => {
                this.recordFromLocation(ressource.type, ressource.location);
            });
        }
    }

    recordFromContent(name, content) {
        this.record(name, this.createParser(content));
    }

    recordFromLocation(name, location) {
        this.recordFromContent(name, require(location));
    }

    record(type, $constructor) {
        this.container[type] = $constructor;
    }

    clear(type) {
        delete this.container[type];
    }

    createParser(ressource) {
        return settings => {
            let entity = null
            
            if (typeof settings.app === 'undefined') {
                settings.app = this.app;
            }

            try {
                entity = new ressource(settings);
            }

            catch (error) {
                this.notifyError(error);
            }

            finally {
                return entity;
            }
        };
    }

    get(type) {
        let parser =  this.container[type];
        if (typeof parser ==='undefined') { 
            const ressource = this.app.getRessource(type);

            if (ressource) {
                parser = this.createParser(ressource);
                this.record(type, parser);
            }
        }

        
        return parser;
    }
}

module.exports = ParserService;