const EntityService = require('../entity');
const configuration = require('./skill.service.config');

class SkillService extends EntityService {
    constructor(settings = {}) {
        settings = Object.assign({}, configuration, settings);
        super(settings);
    }

    getByName(name) {
        return this.app.getSkill(name);
    }
}


module.exports = SkillService;
