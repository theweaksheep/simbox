const EntityService = require('../entity');
const configuration = require('./society.service.config');

class SocietyService extends EntityService {
    constructor(settings = {}) {
        super(Object.assign({}, configuration, settings));
        this.store('type', 'society');
        this.checkDependencies();
    }

    checkDependencies() {
        const ressources = this.app.listRessources();
        if (ressources.indexOf('society') < 0) {
            this.app.getRessource('society');
        }
    }

    onCreateNewInstanceInDB(society, settings) {
        society.name = settings.name;
        society.created_at = Date.now();
        society.status = "sleep";
        society.context = {};
    }

    
    async loadPopulationFor(society) {
        try {
            await this.services.agent.loadMultiples(society.population);
        }

        catch (error) {
            this.notifyError(error);
        }

        finally {
            return true;
            // ... 
        }   
    }

    async launch(society) {
        // batch all agents loading in one request for better performances 
        try {
            await this.loadPopulationFor(society);
        }

        catch(error) {
            this.notifyError(error);
        }

        try {
            await this.services.agent.launchAllById(society.population);
        }

        catch(error) {
            this.notifyError(error);
        }
        
        return true;
    }

    async updateStatus(agent_id, status) {
        let agent_data = await this.retrieveById(agent_id);
        if (
            agent_data !== null && 
            agent_data.status !== status
        ) {
            agent_data.status = status;			
            agent_data = await agent_data.save();

            const agent = this.getById(agent_data._id);
            const newStatus = agent_data.status; 
            
            if (newStatus === 'active') {
                // instance is currently loaded in server
                if (typeof agent !== 'undefined') {
                    agent.status = agent_data.status;
                } 

                // instance is not loader, we have to load her
                else {
                    await this.load(agent_data._id);
                }
            }

            // remove instance from server
            if (
                newStatus === 'sleep' && 
                typeof agent !== 'undefined'
            ) {
                this.killById(agent._id);
            }
        }
        
        return agent;
    }

    async updateContext(society_id, property, value) {
        let society_data = null;
        
        try {
            society_data = await this.retrieveById(society_id);
        }
        
		catch(error) {
            this.notifyError(error, true);
		}
        
        if (society_data === null) {
            this.notifyError(`society not found with id ${society_id}`, true);
        }


        // really update in DB
        if (typeof society_data.context === 'string') {
            const str = society_data.context;
            society_data.context = {
                string: society_data.context
            };
        }

        society_data.context[property] = value;
            
        
        try {
            society_data.markModified('context');
            await society_data.save();
        }

        catch(error) {
            this.notifyError(error, true);
        }
        
        // update name in server agent pool
        let society = this.getById(society_id); 
        if (society) {
            society.context = society.context;
        }

        return society_data;
    }

    async addToPopulationInDB(society_id, agent_id) {
        const society_data = await this.retrieveById(society_id);
        society_data.population.push(agent_id);
        return await society_data.save();
    }

    async addToPopulation(society_id, agent_id) {
        const society = this.getById(society_id);
        if (society !== null) {
            society.population.push(agent_id);

            // should i update the db ?
            if (society.isStored === true) {
                await Promise.all([
                    this.addToPopulationInDB(society_id, agent_id),
                    this.services.agent.setSociety(agent_id, society_id, true)
                ]);
            }

            else {
                this.services.agent.setSociety(agent_id, society_id, false);
            }
        }

        else {
            try {
                await Promise.all([
                    this.addToPopulationInDB(society_id, agent_id),
                    this.services.agent.setSociety(agent_id, society_id, true)
                ]);
            }

            catch(error) {
                this.notifyError(error);
            }
        }

        return society;
    }
    

    async removeFromPopulationInDB(society_id, agent_id) {
        const society_data = await this.retrieveById(society_id);
        society_data.population.push(agent_id);
       
        const index = society_data.population.indexOf(agent_id);
        
        if (index >= 0) {
            society_data.population.splice(index, 1);
            await society_data.save();
        }
    }

    async removeFromPopulation(society_id, agent_id, deepDelete = true) {
        // this.services.agent.leaveSociety(agent_id, society_id, deepDelete);

        const society = this.getById(society_id);

        if (society === null) {
            throw new Error(`No instance of society found with id ${society_id}`);
        }

        const index = society.population.indexOf(agent_id);
        
        if (index >= 0) {
            society.population.splice(index, 1);
        }

        else {
            throw new Error(`No instance of agent (${agent_id}) found in society (${society_id})`);
        }

        if (
            deepDelete === true &&
            society.isStored === true
        ) {
            await this.removeFromPopulationInDB(society_id, agent_id)
        }

        return true;
    }

    async onDelete(society_data) {
        await Promise.all(society_data.population.map(agent_id => {
            return this.services.agent.delete(agent_id);
        }));
    }
    


    /**
     * Update status of society and load it in server if it become active, or kill instance if is sleep
     * @param {ObjectId} society_id id of a society
     * @param {String} status status to set (active or sleep)  
     */
    async updateStatus(society_id, status) {
        let society = await this.retrieveById(society_id);
        
        if (
            society !== null && 
            society.status !== status
        ) {
			society.status = status;			
            society = await society.save();

            const instance = this.getById(society._id);
            const newStatus = society.status; 
            
            if (newStatus === 'active') {
                // instance is currently loaded in server
                if (instance !== null) {
                    instance.status = society.status;
                } 

                // instance is not loader, we have to load her
                else {
                    await this.downloadById(society._id);
                }
            }

            // remove instance from server
            if (
                newStatus === 'sleep' && 
                typeof instance !== 'undefined'
            ) {
                this.killById(society._id);
            }
        }
        
        return society;
    }

    fromJSON(json_data) {
        const society = this.createInstanceFromJSON(json_data);
        
        json_data.agents.forEach(agent => {
            const instance = this.services.agent.createInstanceFromJSON(agent);
            society.addAgent(instance._id);
        });
    
        json_data.links.forEach(key => {
            try {
                this.services.link.fromKey(key);
            }

            catch(error) {
                this.notifyError(error);
            }
        });

        return society;
    }
}

module.exports = SocietyService;