'use strict';
// use object-composition, not inheritancy 
const Message = require('../types/message');
const Skill = require('./skill');

class Receiver extends Skill {
	constructor(settings = {}) {
		settings = Object.assign({}, {
			name: 'receiver'
		}, settings);
		
		super(settings);
	}

	attach(instance) {
		instance.skills[this.name] = {
			registers: {},
			responsesTable: {},
			numberOfRegisters: 0,
			waitingForRegisterNumber: 0,

			prepareNewSequence: this.prepareNewSequence.bind(instance),
			clearResponseTable: this.clearResponseTable.bind(instance),
			isRegistered: this.isRegistered.bind(instance),
			waitResponseOf: this.waitResponseOf.bind(instance),
			register: this.register.bind(instance),
			handleNewMessage: this.handleNewMessage.bind(instance),
			unregister: this.unregister.bind(instance)
		};

		instance.event.on('newMessage', message_id => {
			const message = Message.getById(message_id);
			// send a copy to be sure that this message will not be affected by another feature
			instance.skills.receiver.handleNewMessage(message.copy());
		});
	
	}

	prepareNewSequence() {
		const context = this.skills.receiver;
		context.waitingForRegisterNumber = context.numberOfRegisters;
		context.clearResponseTable();
	}

	clearResponseTable() {
		const context = this.skills.receiver;
		Object.values(context.responsesTable).forEach(message => {
			message.delete();
		});

		context.responsesTable = {};
	}

	isRegistered(entity_id) {
		const context = this.skills.receiver;
		return typeof context.registers[entity_id] !== 'undefined';
	}

	waitResponseOf(entity_id) {
		const context = this.skills.receiver;
		return typeof context.responsesTable[entity_id] === 'undefined';
	}

	register(entity = {}) {
		const context = this.skills.receiver;
		if (!context.isRegistered(entity._id)) {
			context.registers[entity._id] = entity;
			context.numberOfRegisters++;
			if (this.onRegister) {
				this.onRegister(entity);
			}
			try {
				if (entity.hasSkill('publisher')) {
					entity.skills.publisher.addReceiver(this);
				}
			}

			catch(error) {
				this.notifyError(error);
			}
		}
	}

	handleNewMessage(message) {
		const context = this.skills.receiver;
		const sender_id = message.getSender();
		if (
			context.waitingForRegisterNumber === 0 && 
			context.numberOfRegisters > 0
		) {
			context.prepareNewSequence(); 
		}

		if (
			context.isRegistered(sender_id) && 
			context.waitResponseOf(sender_id) && 
			context.waitingForRegisterNumber > 0
		) {
			context.responsesTable[sender_id] = message;
			context.waitingForRegisterNumber--;
		}

		if (
			context.waitingForRegisterNumber === 0 && 
			context.numberOfRegisters > 0
		) {
			this.event.emit('messageFromAllRegisters');
		}

	}
	
	
	unregister(entity) {
		const context = this.skills.receiver;
		if (context.isRegistered(entity._id)) {
			delete context.registers[entity._id];
			context.numberOfRegisters--;

			if (entity.hasSkill('publisher')) {
				entity.skills.publisher.removeReceiver(this);
			}
		}
	}
}

module.exports = Receiver;

