// use object-composition, not inheritancy
const Skill = require('../skill');

const skillName = 'all-actives-slots-must-be-ready';
const version = '1.0';

class ActiveSlotReady extends Skill {
	constructor() {
        super({
            name: skillName,
            version: version
        });
	}

	attach(instance) {
		instance.skills[this.name] = {
            check: this.check.bind(instance),
            version: this.version
        }
	}

    async check(results) {
        if (this.hasSkill('slot-io') === false) {
            this.notifyError(`${skillName} require @skill:slot-io to run`, true);
            return false;
        }


        const input_slots =  this.core.slots.input;

        let allActiveAreReady = true;
        Object.values(input_slots).forEach(slot => {
            if (slot.active === true && slot.value === null) {
                allActiveAreReady = false;
            }
        });

        return allActiveAreReady;
    }
}

module.exports = ActiveSlotReady;