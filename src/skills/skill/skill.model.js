const Entity = require('../../types/entity/entity.model');
const configuration = require('./skill.model.config');

class Skill extends Entity {
    constructor(settings = {}) {
        settings = Object.assign({}, configuration, settings);
        super(settings);

        this.version = settings.version
    }

    attach(another_object) {
        // add skill here
    }
}

module.exports = Skill;