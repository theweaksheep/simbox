// use object-composition, not inheritancy
const Skill = require('../skill');
const Message = require('../../types/message');

class MessageManagerOutput extends Skill {
	constructor() {
        super({
            name: 'output-sending-message-manager'
        });
	}

	attach(instance) {
		instance.skills[this.name] = {
			send: this.send.bind(instance)
        }
	}

    async send(timestamp) {
        const message = new Message({
            timestamp: this.core.lastOutput.timestamp,
            content: { ...this.core.lastOutput.value },
            sender_id: this._id
        });

        if (this.hasSkill('message-manager')) {
            this.messageManger.send(result);
        }

        else {
            this.notifyError('[message-manager] skill is required to use [use-message-manager-for-output]');
        }
    }
}

module.exports = MessageManagerOutput;