// use object-composition, not inheritancy
const Skill = require('../skill');
const Slot = require('../../types/slot');


const skillName = 'only-alter-can-input-slots';
class AllowedSlots extends Skill {
	constructor() {
        super({
            name: skillName,
            version: '1.0'
        });
	}

	attach(instance) {
		instance.skills[this.name] = {
            check: this.check.bind(instance),
            version: this.version
        }
	}

    check(message = {}) {
        const link_id = message.content.link_id;
        const link = this.services.link.getById(link_id);
        const slot = this.services.slot.getById(link.input_slot_id);
    
        if (slot === null) {
            this.notifyError(`@${skillName} slot not found using link : ${link_id}`);
            return false;
        }

        if (
            typeof slot === 'undefined' ||
            slot === null ||
            slot.checkLinkSanity(link_id) === false
        ) {
            this.notifyError(`@${skillName} unsane link : ${link_id}`);
            return false;
        }

        slot.storeValue(message.content.value, message.getTimestamp());
        return true;
    }
}

module.exports = AllowedSlots;