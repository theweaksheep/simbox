// use object-composition, not inheritancy
const Skill = require('../skill');
const Agent = require('../../types/agent');


class Timestamp extends Skill {
	constructor() {
        super({
            name: 'message-same-timestamp'
        });
	}

	attach(instance) {
		instance.skills[this.name] = {
            timestamp: 0,
            check: this.check.bind(instance),
            searchOutdatedPendingInput: this.searchOutdatedPendingInput.bind(instance),
            askUpdate: this.askUpdate.bind(this)
        }
	}

    searchOutdatedPendingInput(timestamp) {
        const pending_inputs = Object.values(this.core.pending_inputs);

        return pending_inputs.filter(pending_message => {
            return pending_message.getTimestamp() === timestamp;
        });
    }

    askUpdate(outadedPendingInput, timestamp) {
        outadedPendingInput.forEach(outaded_message => {
            const jobber = Agent.getById(outaded_message.getSender());
            jobber.resolve_rules('output_outdated', timestamp);
        });
    }

    async check(message = {}) {
        const context = this.skills['message-same-timestamp'];
        const timestamp = message.getTimestamp();
        if (!message.hasTimestamp()) {
            this.notifyError('invalid timestamp :', timestamp);
        }

        // refuse outdated input
        if (timestamp < context.timestamp) {
            return false;
        }

        // skill-context timestamp need to be updated ?
        if (timestamp > context.timestamp) {
            context.timestamp = timestamp;
        }

        // search pending-input with outdated timestamp
        const inputs_outdated = context.searchOutdatedPendingInput(timestamp);

        if (inputs_outdated.length === 0) {
            if (this.core.pending_inputs[message.sender_id]) {
                this.core.pending_inputs[message.sender_id].delete();
            }
            
            this.core.pending_inputs[message.sender_id] = message;
            
            return true;
        }

        // some pending input need to be updated to prevent dephasage
        else {
            context.askUpdate(inputs_outdated, timestamp);
            return false;
        }
    }
}

module.exports = Timestamp;