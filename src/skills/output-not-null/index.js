// use object-composition, not inheritancy
const Skill = require('../skill');
// const isObjectEmpty = require('@tools/isObjectEmpty')

class NotNull extends Skill {
	constructor() {
        super({
            name: 'output-not-null'
        });
	}

	attach(instance) {
		instance.skills[this.name] = {
			check: this.check.bind(instance)
        }
	}

    async check(message = null) {

        if (message === null) {
            return false;
        }
        
        else {
            return true;
        }
    }
}

module.exports = NotNull;