// use object-composition, not inheritancy
const Message = require('../../types/message');
const Skill = require('../skill');

const configuration = require('./forward-output-using-slot.config');
const skillName = configuration.name;

class OuputSlots extends Skill {
	constructor() {
        super(configuration);
	}

	attach(instance) {
		instance.skills[this.name] = {
            send: this.send.bind(instance),
            version: this.version
        }
	}

    send(results) {
        if (this.hasSkill('slot-io') === false) {
            this.notifyError(`${skillName} require @skill:slot-io to run`, true);
            return false;
        }

        const output_slots =  this.core.slots.output;

        if (Object.keys(output_slots).length <= 0) {
            return;
        } 

        Object.keys(results).forEach(slotName => {
            const slot =  output_slots[slotName];
            if (typeof slot === 'undefined') {
                this.notifyError(`@${skillName} output-slot not found : ${slotName}. Candidats are : ${Object.keys(output_slots)}`);
                return;
            }

            Array.from(slot.link_id).forEach(link_id => {
                const link = this.services.link.getById(link_id);
                
                const message = new Message({
                    content: {
                        link_id: link_id,
                        value: results[slotName]
                    },
    
                    timestamp: this.core.lastOutput.timestamp,
                    sender_id: this._id,
                    receiver_id: link.input_owner_id
                });
    
                this.send(message);
            });
            
        });
    }
}

module.exports = OuputSlots;