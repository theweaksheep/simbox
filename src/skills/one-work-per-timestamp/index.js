// use object-composition, not inheritancy
const Skill = require('../skill');

const skillName = 'one-work-per-timestamp';

class triggerSkill extends Skill {
	constructor() {
        super({
            name: 'one-work-per-timestamp',
            version: '1.0'
        });
	}

	attach(instance) {
		instance.skills[this.name] = {
			check: this.check.bind(instance)
        }
	}

    async check(message = {}) {
        const timestamp = message.getTimestamp();
        if (timestamp > this.core.lastTrigger) {
            this.core.lastTrigger = timestamp;
            return true;
        }

        else {
            return false;
        }
    }
}

module.exports = triggerSkill;