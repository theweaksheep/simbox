// use object-composition, not inheritancy
const Skill = require('../skill');

const skillName = 'slot-io';
class SlotSystem extends Skill {
	constructor(settings) {
        super({
            name: skillName,
            version: '1.0',
            ...settings
        });
	}

	

	attach(instance) {
		instance.skills[skillName] = {
            initialize: this.initialize.bind(instance),
            retrieveSlotsFromContext: this.retrieveSlotsFromContext.bind(instance),
            addSlot: this.addSlot.bind(instance),
            version: this.version
        }
	}

    initialize(settings = {}) {
        const skill = this.skills[skillName];
        if (typeof this.core.slots === 'undefined') {
            this.core.slots = {
                input: {},
                output: {}
            };
        }

        if (typeof settings.context !== 'undefined') {
            skill.retrieveSlotsFromContext(settings.context.slots);
        }

        if (typeof settings.slots !== 'undefined') {
            skill.retrieveSlotsFromContext(settings.slots);
        }
    }

    addSlot(type, slotData, slot_name) {
        slotData.owner_id = this._id;
        slotData.type = type;
        slotData.name = slotData.name || slot_name;
        this.core.slots[type][slot_name] = this.services.slot.create(slotData);
    }

    retrieveSlotsFromContext(slots = {}) {
        const skill = this.skills[skillName];
        if (typeof slots.input !== 'undefined') {
            Object.keys(slots.input).forEach(slot_name => {
                skill.addSlot('input', slots.input[slot_name], slot_name);
            });
        }

        if (typeof slots.output !== 'undefined') {
            Object.keys(slots.output).forEach(slot_name => {
                skill.addSlot('output', slots.output[slot_name], slot_name);
            });
        } 
    }
}

module.exports = SlotSystem;
