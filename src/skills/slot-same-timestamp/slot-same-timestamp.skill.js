// use object-composition, not inheritancy
const Skill = require('../skill');
const configuration = require('./slot-same-timestamp.skill.config');

class Timestamp extends Skill {
	constructor(settings = {}) {
        super(Object.assign({}, configuration, settings));
	}

	attach(instance) {
		instance.skills[this.name] = {
            timestamp: 0,
            check: this.check.bind(instance),
            searchOutdatedSlot: this.searchOutdatedSlot.bind(instance),
            askUpdate: this.askUpdate.bind(instance),
            searchOutdated: (agent, timestamp) => {
                return this.searchOutdatedSlot.call(agent, timestamp);
            }
        }
	}

    searchOutdatedSlot(timestamp) {
        this.debug('search outdated', timestamp)
        const pending_slots = Object.values(this.getInputActivesSlots());

        return pending_slots.filter(pending_slot => {
            this.debug(pending_slot.name, pending_slot.timestamp, timestamp);
            return pending_slot.timestamp !== timestamp;
        });
    }

    askUpdate(outadedPendingInput, timestamp) {
        const context = this.skills['slot-same-timestamp'];

        const propage = (input_slot) => {
            const link_id = input_slot.link_id.values().next().value;
            const link = this.services.link.getById(link_id);

            if (input_slot.type !== 'input') {
                throw new Error('The slot is not an input slot (slot_id:)' + input_slot._id);
            }
          
            // propage backward, receiver is output-slot
            const jobber = this.services.agent.getById(link.output_owner_id);
            jobber.resolve_rules('output-outdated', {
                timestamp: timestamp,
                searchOutdated: context.searchOutdated,
                propage: propage
            });
        };

        outadedPendingInput.forEach(slot => {
            propage(slot);            

            // jobber.resolve_rules('output-outdated', {
            //     timestamp: timestamp,
            //     searchOutdated: searchOutdated,

            //     propage: 
            // });
        });
    }

    async check(message = {}) {
        const context = this.skills['slot-same-timestamp'];
        const timestamp = message.getTimestamp();

        if (!message.hasTimestamp()) {
            this.notifyError('invalid timestamp :', timestamp, context.timestamp);
        }

        // refuse outdated input
        if (timestamp < context.timestamp) {
            return false;
        }

        // skill-context timestamp need to be updated ?
        if (timestamp > context.timestamp) {
            context.timestamp = timestamp;
        }

        // search pending-input with outdated timestamp
        const inputs_outdated = context.searchOutdatedSlot(context.timestamp);

        if (inputs_outdated.length === 0) {            
            return true;
        }

        // some pending input need to be updated to prevent dephasage
        else {
            context.askUpdate(inputs_outdated, context.timestamp);
            return false;
        }
    }
}

module.exports = Timestamp;