const EntityController = require('../entity');
const configuration = require('./agent.controller.config');

class AgentController extends EntityController {
    constructor(settings = {}) {
        // Merge default configuration file with settings 
        settings = Object.assign({}, configuration, settings);
        super(settings);
    }

    getAll() {
        const response = this.prepareResponse();

        try {
            const fullAgentsArray = Object.values(this.services.manager.getAll());
            
            // reduce agent data to avoid circular references
            response.message = fullAgentsArray.map(agent => {
                return { 
                    name: agent.name, 
                    type: agent.type, 
                    context: agent.context, 
                    society_id: agent.society_id, 
                    status: agent.status 
                };
            });

            response.error = false;  
        }

        catch (error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    async create(settings) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.createAndStore(settings);
            response.error = false;  
        }

        catch (error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    async createAgentInDB(settings) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.createInDB(settings);
            response.error = false;  
        }

        catch (error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    async updateStatus(agent_id, status) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.updateStatus(agent_id, status);
            response.error = false;  
        }

        catch (error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    replaceSlot(agent, type, key, slot) {
        const lastState = JSON.parse(JSON.stringify(agent.slots[type][key]));
        delete agent.slots[type][key];
        const entry = agent.slots[type];
        // search in all input / output slot if from / to agent is still connected
        if (type === 'input') {
            const slotsName = Object.keys(entry);
            let stillHere = false;
            slotsName.forEach(name => {
                const s = entry[name];
                if (s.from === lastState.from) {
                    stillHere = true;
                } 
            });

            if (stillHere === false) {
               agent.inner.delete(lastState.from); 
            }

            agent.inner.add(slot.from);
        }

        if (type === 'output') {
            const slotsName = Object.keys(entry);
            let stillHere = false;
            slotsName.forEach(name => {
                const s = entry[name];
                if (s.to === lastState.to) {
                    stillHere = true;
                } 
            });

            if (stillHere === false) {
                agent.outer.delete(lastState.to);
            }
        }

        agent.slots[type][key] = slot;

        return agent;
    }

    async updateSlot(agent_id, type, key, slot) {
        // update slot instance
        const instance = replaceSlot(this.services.manager.getById(agent_id), type, key, slot);
        const agentData = await this.services.manager.retrieveById(agent_id);
        
        agentData.inner = Array.from(instance.inner);
        agentData.outer = Array.from(instance.outer);
        
        if (typeof agentData.context.slots[type] === 'undefined') {
            agentData.context.slots[type] = {};
        }

        agentData.context.slots[type][key] = slot;

        agentData.markModified('context');
        return await agentData.save();
    }

    async updateContext(agent_id, property, value) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.updateContext(agent_id, property, value);
            response.error = false;  
        }

        catch(error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    async addToSocietyInDB(agent_id, society_id) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.addToSocietyInDB(agent_id, society_id);
            response.error = false;  
        }

        catch(error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    // static async removeInnerLiaisonWith(agent_id) {
    //     const receivers = await AgentDB.find({ inner: agent_id });
        
    //     receivers.forEach(async (receiver) => {
    //         const index = receiver.inner.indexOf(agent_id);
    //         if (index >= 0) {
    //             receiver.inner.splice(index, 1);
    //             await receiver.save();
    //             const instance = Agent.getById(receiver._id); 
    //             if (instance) {
    //                 instance.removeInner(agent_id);
    //             }
    //         }
    //     });
    // }

    // static async removeOuterLiaisonWith(agent_id) {
    //     const publishers = await AgentDB.find({ outer: agent_id });

    //     publishers.forEach(async (publisher) => {
    //         const index = publisher.outer.indexOf(agent_id);
    //         if (index >= 0) {
    //             publisher.outer.splice(index, 1);
    //             await publisher.save();
    //             const instance = Agent.getById(publisher._id); 
    //             if (instance) {
    //                 instance.removeOuter(agent_id);
    //             }
    //         }
    //     });
    // }

    async updateType(agent_id, type) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.updateType(agent_id, type);
            response.error = false;  
        }

        catch(error) {
           this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    async removeFromSociety(agent_id) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.removeFromSociety(agent_id, type);
            response.error = false;  
        }

        catch(error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    // static addOuter(agent_id, outer_id) {
    //     // todo add redundance test
    //     // todo update db
    //     const agent = Agent.getById(agent._id);
    //     const outer = Agent.getById(outer_id);

    //     if (agent && outer) {
    //         agent.addOuter(outer);
    //         outer.addInner(agent);
    //     }
    // }

    // static addInner_instance(agent_id, inner_id) {
    //     const agent = Agent.getById(agent._id);
    //     const inner = Agent.getById(inner_id);

    //     if (agent && inner) {
    //         agent.addInner(inner);
    //         inner.addOuter(agent);
    //     }
    // }

    // static async removeInner(agent_id, inner_id) {
    //     const agent = await AgentDB.findById(agent_id);
    //     const index = agent.inner.indexOf(inner_id);

    //     if (index >= 0) {
    //         agent.inner.splice(index, 1);
    //         await agent.save();
    //         const instance = Agent.getById(agent._id); 
    //         if (instance) {
    //             instance.removeInner(inner_id);
    //         }
    //     }
    // }

    // static async removeOuter(agent_id, outer_id) {
    //     const agent = await AgentDB.findById(agent_id);
    //     const index = agent.outer.indexOf(outer_id);

    //     if (index >= 0) {
    //         agent.outer.splice(index, 1);
    //         await agent.save();
    //         const instance = Agent.getById(agent._id); 
    //         if (instance) {
    //             instance.removeOuter(outer_id);
    //         }
    //     }
    // }
}

module.exports = AgentController;