const EntityController = require('../entity');
const configuration = require('./society.controller.config');

class SocietyController extends EntityController {
    constructor(settings = {}) {
        // Merge default configuration file with settings 
        settings = Object.assign({}, configuration, settings);
        super(settings);
    }

    async create(settings) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.createAndStore(settings);
            response.error = false;  
        }

        catch (error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    async createInDB(settings) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.createInDB(settings);
            response.error = false;  
        }

        catch (error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    async updateContext(society_id, name, value) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.updateContext(society_id, name, value);
            response.error = false;  
        }

        catch (error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    async addNewAgent(settings = {}) {
        const response = this.prepareResponse();

        try {

            const agent_data = await this.services.agent.createAndStore(settings.agent);
            await this.services.manager.addToPopulation(settings.society_id, agent_data._id);
            response.message = agent_data;
            response.error = false;  
        }

        catch (error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    async addToPopulationInDB(society_id, agent_id) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.addToPopulationInDB(society_id, agent_id);
            response.error = false;  
        }

        catch (error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    async addToPopulation(society_id, agent_id) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.addToPopulation(society_id, agent_id);
            response.error = false;  
        }

        catch (error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }
    

    async removeFromPopulationInDB(society_id, agent_id) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.removeFromPopulationInDB(society_id, agent_id);
            response.error = false;  
        }

        catch (error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }
    
    async removeFromPopulation(society_id, agent_id, deep = false) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.removeFromPopulation(society_id, agent_id, deep);
            response.error = false;  
        }

        catch (error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }


    /**
     * Update status of society and load it in server if it become active, or kill instance if is sleep
     * @param {ObjectId} society_id id of a society
     * @param {String} status status to set (active or sleep)  
     */
    async updateStatus(society_id, status) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.updateStatus(society_id, status);
            response.error = false;  
        }

        catch (error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }
}



module.exports = SocietyController;