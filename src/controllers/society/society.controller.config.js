module.exports = {
    name: 'society-controller',

    services: {
        // give the name of service and it will be auto-loaded
        manager: 'society',
        agent: 'agent'
    }
};