const AgentController = require('../agent');
const Slot = require('../../types/slot');

/*
- Ajouter / retirer link (params: output_agent_id, output_slot_name, input_agent_id, input_slot_name)

- Changer la valeur par défaut (agent_id, slot_name, value)

- Désactiver un link (sans le supprimer) (link_id)

*/
class SlotBoxController extends AgentController {
    static async updateDefaultValue(sb_id, type, slot_name, value) {
        const sb = AgentController.retrieveInstance(sb_id);
        const slot = sb.core.slots[type][slot_name];
        if (slot.defaultValue === value) {
            return {
                error: false,
                message: 'ok'
            };
        }

        slot.defaultValue = value;
        const data = AgentController.getById(sb._id);
        data.context.slots[type][slot_name].defaultValue = value;
        sb.markModified('context');
        
        try {
            await sb.save();
        }
        
        catch(error) {
            return {
                error: true,
                message: error
            };
        }

        return {
            error: false,
            message: 'ok'
        };
        
    }

    static async link(outer_id, outer_slot_name, inner_id, inner_slot_name) {
        const outer_instance = AgentController.retrieveInstance(outer_id);
        const inner_instance = AgentController.retrieveInstance(inner_id);
        
        const outer_slot = outer_instance.core.slots.output[outer_slot_name];
        const inner_slot = inner_instance.core.slots.output[inner_slot_name];
        const updatedSlots = new Set();
        outer_slot.link(inner_slot, updatedSlots);

        try {
            await Promise.all(updatedSlots.map(slot => new Promise(async(resolve, reject) => {
                const sb = await AgentController.getById(slot.owner_id);
                const slotData = sb.context.slots[slot.type][slotsName];
                slotData.link_id = slot.link_id;
                slotData.alter_id = slot.alter_id;
    
                sb.context.slots[slot.type][slotsName] = slotData;
                sb.markModified('context');
                sb.save((err, doc) => {
                    if (err) reject(err);
                    else resolve(doc);
                });
            })));
        }
        
        catch(error) {
            return {
                error: true,
                message: error
            }
        }

        return {
            error: false,
            message: 'ok'
        }
    }

    static async unlink(link_id) {
        const link = Slot.findByLink(link_id);
        const outer_slot = link.output;
        
        
        const updatedSlots = new Set();
        if (link !== null) {
            link.output.unlink(updatedSlots);
            link.input.unlink(updatedSlots);
        }

        try {
            await Promise.all(updatedSlots.map(slot => new Promise(async (resolve, reject) => {
                const sb = await AgentController.getById(slot.owner_id);
                const slotData = sb.context.slots[slot.type][slotsName];
                slotData.link_id = slot.link_id;
                slotData.alter_id = slot.alter_id;
    
                sb.context.slots[slot.type][slotsName] = slotData;
                sb.markModified('context');
                sb.save((err, doc) => {
                    if (err) reject(err);
                    else resolve(doc);
                });        
            })));
        }
        
        catch(error) {
            return {
                error: true,
                message: error
            }
        }

        return {
            error: false,
            message: 'ok'
        }
    }
}

module.exports = SlotBoxController;