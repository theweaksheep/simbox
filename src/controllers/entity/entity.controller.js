const Entity = require('../../types/entity');
const configuration = require('./entity.controller.config');

class EntityController extends Entity {
    constructor(settings = {}) {
        // Merge default configuration file with settings 
        settings = Object.assign({}, configuration, settings);

        super(settings);
    }

    setupServices(services = {}) {
        super.setupServices(services);

        if (typeof this.services.manager === 'undefined') {
            this.notifyError('A controller must have a manager-service', true);
        }
    }


    async create(settings) {
        
    }
    
    async getByName(name) {
        const response = this.prepareResponse();
        try {
            response.message = await this.services.manager.getByName(name);
            response.error = false;
        }

        catch(error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    async getById(entity_id) {
        const response = this.prepareResponse();
        try {
            response.message = await this.services.manager.getById(entity_id);
            response.error = false;
        }

        catch(error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }
    

    getAll() {
        const response = this.prepareResponse();
        try {
            response.message = this.services.manager.getAll();
            response.error = false;
        }

        catch(error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    onError(error, response) {
        this.notifyError(error);
        response.error = true;
        response.message = error.message;
    }

    async retrieveAll() {
        const response = this.prepareResponse();
        try {
            response.message = await this.services.manager.retrieveAll();
            response.error = false;
        }

        catch(error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    async rename(entity_id, name) {
        const response = this.prepareResponse();
        try {
            response.message = await this.services.manager.rename(entity_id, name);
            response.error = false;
        }

        catch(error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    prepareResponse() {
        return {
            error: true,
            message: 'unexpected error'
        };
    }

    async load(entity_id) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.downloadById(entity_id);
            response.error = false;  
        }

        catch(error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    async loadMultiples(arrayOfID) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.loadMultiples(arrayOfID);
            response.error = false;  
        }

        catch(error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    async delete(entity_id) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.delete(entity_id);
            response.error = false;  
        }

        catch(error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    async removeInDB(entity_id) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.removeInDB(entity_id);
            response.error = false;  
        }

        catch(error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    async retrieveByName(name) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.retrieveByName(name);
            response.error = false;  
        }

        catch(error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    async retrieveById(entity_id) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.retrieveById(entity_id);
            response.error = false;  
        }

        catch(error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    async retrieveInstance(entity_id) {
        const response = this.prepareResponse();

        try {
            response.message = await this.services.manager.retrieveInstance(entity_id);
            response.error = false;  
        }

        catch(error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }

    

    killById(entity_id) {
        const response = this.prepareResponse();

        try {
            response.message = this.services.manager.killById(entity_id);
            response.error = false;  
        }

        catch(error) {
            this.onError(error, response);
        }

        finally {
            return response;
        }
    }
}

module.exports = EntityController;