const SlotBox = require('../slot-box');

class AdditionBox extends SlotBox {
    constructor(settings = {}) {
        super(settings);
    }
    
    checkSlotsConstraints(settings) {
        this.mustHaveInput('valueA');
        this.mustHaveInput('valueB');
    }

    onJob(message) {
        const valueA = this.getInput('valueA');
        const valueB = this.getInput('valueB');

        return  valueA + valueB;
    }
}

module.exports = AdditionBox;