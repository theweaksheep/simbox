const SlotBox = require('../slot-box');

class HeraldBox extends SlotBox {
    constructor(settings = {}) {
        super(settings);
        this.store('value', 'faut pas respirer de la compote ça fait tousser');
    }

    checkSlotsConstraints(settings) {
        if (typeof this.core.slots.output.message === 'undefined') {
            // this.notifyError('HeraldBox must have an input-slot named "message"');
            this.createSlot({
                type: 'output',
                name: 'message'
            });
        } 
    }


    onJob() {
        return this.data.value;
    }

    static create(data) {
        let instance;

        try {
            instance = new HeraldBox(data);
        } 

        catch(e) {
            this.notifyError(error);
        }

        return instance;
    }
}

module.exports = HeraldBox;