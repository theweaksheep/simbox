const SlotBox = require('../slot-box');

class LoggerBox extends SlotBox {
    constructor(settings = {}) {
        super(settings);
    }

    checkSlotsConstraints(settings) {
        if (typeof this.core.slots.input.message === 'undefined') {
            // this.notifyError('HeraldBox must have an input-slot named "message"');
            this.createSlot({
                type: 'input',
                name: 'message'
            });
        } 
    }


    onJob() {
        this.log(this.getInputSlot('message').getValue());
    }

    static create(data) {
        let instance;

        try {
            instance = new LoggerBox(data);
        } 

        catch(e) {
            this.notifyError(error);
        }

        return instance;
    }
}

module.exports = LoggerBox;