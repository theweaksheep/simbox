const Jobber = require('../jobber');
const Message = require('../message');

/**
 * Create a class of agent using skills Receiver and Publisher 
 */

class SocietyVertex extends Jobber {
    constructor(settings = {}) {
        super(settings);

        this.addRule('setup', 'receiver');
        this.addRule('setup', 'publisher');
        this.addRule('output-sending', 'publisher');
    }


    onSetup(settings = {}) {
        super.onSetup(settings);

        this.inner = new Set((typeof settings.inner !== 'undefined') ? settings.inner : []);
        this.outer = new Set((typeof settings.outer !== 'undefined') ? settings.outer : []);
    }


    live() {
        super.live();
    }

    onLive() {
        this.outer.forEach((agent_id) => {
            this.skills.publisher.addReceiver(
                this.services.agent.getById(agent_id)
            );
        });

        this.inner.forEach(agent_id => {
            this.skills.receiver.register(
                this.services.agent.getById(agent_id)
            );
        });
    }

    addOuter(outer) {
        this.outer.add(outer._id);
        this.skills.publisher.addRecipient(outer);
    }

    addInner(inner) {
        this.inner.add(inner._id);
        this.skills.receiver.register(inner);
    }

    removeOuter(outer_id) {
        const deleted = this.outer.delete(outer_id);
        if (deleted === true) {
            this.skills.publisher.removeReceiver(
                this.services.agent.getById(agent_id)
            );
        }
    }

    removeInner(inner_id) {
        const deleted = this.inner.delete(inner_id);
        if (deleted === true) {
            this.skills.receiver.unregister(this.services.agent.getById(inner_id));
        }
    }

    prepareOutput() {
        return new Message({
            content: this.core.lastOutput.value,
            timestamp: this.core.lastOutput.timestamp,
            sender_id: this._id
        });
    }
}

module.exports = SocietyVertex;