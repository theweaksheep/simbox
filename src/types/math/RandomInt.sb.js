const SlotBox = require('../slot-box');

class RandomInt extends SlotBox {
    constructor(settings = {}) {
        super({
            strictMode: true,
            ...settings
        });
    }

    checkSlotsConstraints(settings) {
        if (typeof this.core.slots.output.number === 'undefined') {
            this.createSlot({
                type: 'output',
                name: 'number'
            });
        } 
    }

    onJob() {
        return Math.ceil(Math.random() * 100);
    }

    static create(data) {
        let instance;

        try {
            instance = new RandomInt(data);
        } 

        catch(e) {
            this.notifyError(error);
        }

        return instance;
    }
}

module.exports = RandomInt;