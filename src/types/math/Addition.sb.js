
const SlotBox = require('../slot-box');

class AdditionBox extends SlotBox {
    constructor(settings = {}) {
        super(settings);
    }

    /**
     * Méthode invoquée automatiquement à la fin de la création de l'objet.
     * Permet de vérifier l'intégrité de notre instance. Très utile pour le debug, surtout si le créé avec des données issues d'une base. 
     */
    checkSlotsConstraints(settings) {
        if (typeof this.core.slots.input.valueA === 'undefined') {
            this.notifyError('AdditionBox must have an input-slot named "valueA"');
        } 

        if (typeof this.core.slots.input.valueB === 'undefined') {
            this.notifyError('AdditionBox must have an input-slot named "valueB"');
        }
    }

    /**
     * C'est LA méthode qui se charge de faire le sale boulot. La logique de notre boîte est à l'intérieur
     * @param {Message} message Dernier message envoyé 
     */
    onJob(message) {

        // On récupère la valeur qu'il y a dans nos slots
        const valueA = this.core.slots.input.valueA.getValue();
        const valueB = this.core.slots.input.valueB.getValue();

        // On effectue notre traitement
        const result = valueA + valueB;

        // Et on renvoi le tout !
        return result;
    }
}

module.exports = AdditionBox;