const SlotBox = require('../slot-box');

class AndBox extends SlotBox {
    constructor(settings = {}) {
        super(settings);
    }

    checkSlotsConstraints(settings) {
        if (typeof this.core.slots.input.valueA === 'undefined') {
            this.notifyError('AddBox must have an input-slot named "valueA"');
        } 

        if (typeof this.core.slots.input.valueB === 'undefined') {
            this.notifyError('AddBox must have an input-slot named "valueB"');
        }
    }

    onJob(message) {
        const valueA = this.core.slots.input.valueA.getValue();
        const valueB = this.core.slots.input.valueB.getValue();

        return valueA && valueB;
    }

    static create(data) {
        let instance;

        try {
            instance = new AndBox(data);
        } 

        catch(e) {
            this.notifyError(error);
        }

        return instance;
    }
}

module.exports = AndBox;