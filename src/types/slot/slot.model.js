const Entity = require('../entity');
const configuration = require('./slot.model.config');
class Slot extends Entity {
    constructor(settings = {}) {
        super(Object.assign({}, configuration, settings));
        this.active = settings.active || false; 
        this.link_id = new Set();
        this.owner_id = settings.owner_id || null;
        
        this.setName(settings.name);
        this.setType(settings.type);
        this.defaultValue = settings.defaultValue;
        
        this.value = null;
        this.timestamp = null;
    }

    getValue() {
        return this.isReady() ? this.value : this.defaultValue;
    }

    isReady() {
        return (
            this.active === true &&
            this.value !== null
        );
    }

    storeValue(value, timestamp = null) {
        this.value = value;
        this.timestamp = timestamp !== null ? timestamp : Date.now();
    }

    clear() {
        this.value = null;
    }

    getData() {
        return {
            active: this.active, 
            link_id: this.link_id,
            owner_id: this.owner_id,
            name: this.name,
            type: this.type,
            defaultValue: this.defaultValue
        };
    }

    toString() {
        return JSON.stringify(this.getData);
    }
 
    isCompatible(slot) {
        return (
            (this.type === 'input' && slot.type !== 'output') &&
            (this.type === 'output' && slot.type !== 'input')
        );
    }

    onLink(link_id = null, slot = null) {
        if (slot === null) {
            throw new Error('cannot link a null-slot');
        }

        if (this.isCompatible(slot)) {
            throw new Error('incompatible type');
        }

        if (
            this.type === 'input' &&
            this.link_id.length > 0
        ) {
            this.services.link.delete(this.link_id[0]);
        }

        this.link_id.add(link_id)
        this.active = true;
    }

    link(slot) {
        let input_slot, output_slot;

        if (this.type === 'input') {
            input_slot = this;
            output_slot = slot;
        }

        else {
            input_slot = slot;
            output_slot = this;
        }

        return this.services.link.create({
            input_slot_id: input_slot._id,
            input_owner_id: input_slot.owner_id,
            input_slot_name: input_slot.name,
            output_slot_id: output_slot._id,
            output_owner_id: output_slot.owner_id,
            output_slot_name: output_slot.name
        });
    }

    onLinkDeleted(link_id) {
        this.link_id.delete(link_id);
        this.active = this.link_id.length > 0;
    }

    die() {
        this.services.slot.delete(this._id);
    }

    checkLinkSanity(link_id = null) {
        if (link_id === null) {
            const sanity = Array.from(this.link_id).filter(id => this.checkLinkSanity(id) === false);
            return sanity.length === 0;
        }

        const link = this.services.link.getById(link_id);
        const slot_service = this.services.slot;
        
        if (
            link === null ||
            !slot_service.exists(link.input_slot_id) ||
            !slot_service.exists(link.output_slot_id)
        ) {
            return false;
        }

        let relatedSlot = null;

        if (this.type === 'input') {
            relatedSlot = slot_service.getById(link.input_slot_id);
        }

        else {
            relatedSlot = slot_service.getById(link.output_slot_id);   
        }
        
        if (
            this.active !== true ||
            relatedSlot.active !== true
        ) {
            return false;
        }

        return true;
    }

    setName(name = null) {
        this.name = name;
    }


    setType(type = null) {    
        this.type = type;
    }
}




module.exports = Slot;