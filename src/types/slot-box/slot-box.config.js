module.exports = {
    name: 'slot-box',
    
    services: {
        agent: 'agent',
        skill: 'skills',
        link: 'link',
        slot: 'slot'
    },
    
    useRules: {
        setup: [ 'slot-io' ],
        'input-sync': ['slot-same-timestamp'],

        'input-check': [ 
            'only-alter-can-input-slots',
            'input-not-null'
        ],

        'output-outdated': ['output-outdated-resolve-backward'],

        trigger: [
            'all-actives-slots-must-be-ready', 
            'one-work-per-timestamp'
        ],
        
        'output-check': ['output-not-null'],

        

        'output-sending': [
            'forward-output-using-slot'
        ]
    }
};