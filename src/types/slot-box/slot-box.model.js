const Jobber = require('../jobber');
const configuration = require('./slot-box.config');


class SlotBox extends Jobber {
    constructor(settings = {}) {
        super(Object.assign({}, configuration, settings));
        this.checkSlotsConstraints(settings);
    }

    checkSlotsConstraints(settings) {
        
    }

    mustHaveInput(name, createIfUndefined = true) {
        if (typeof this.core.slots.input[name] === 'undefined') {
            if (createIfUndefined === true) {
                this.createSlot({
                    type: 'input',
                    name: name
                });
            }

            else {
                this.notifyError(`${this.constructor.name} must have an input-slot called "${name}"`);
            }
        
        }
    }

    mustHaveOutput(name, createIfUndefined = true) {
        if (typeof this.core.slots.output[name] === 'undefined') {
            if (createIfUndefined === true) {
                this.createSlot({
                    type: 'output',
                    name: name
                });
            }

            else {
                this.notifyError(`${this.constructor.name} must have an output-slot called "${name}"`);

            }
        }
    }

    onJob() {
        return {
            data: 'hello'
        };
    }

    getInputActivesSlots() {
        return Object.values(this.core.slots.input).filter(slot => {
            return slot.active === true
        });
    }

    delete() {
        super.delete();

        const slots = this.core.slots;
        Object.value(slots.input).forEach(slot => slot.remove());
        Object.value(slots.output).forEach(slot => slot.remove());
    }

    getInputSlot(name) {
        const slot = this.core.slots.input[name];
        if (typeof slot === 'undefined') {
            this.notifyError(`There is no input slot called ${name}`);
            return null;
        }

        else {
            return slot;
        }
    }


    getOutputSlot(name) {
        const slot = this.core.slots.output[name];
        if (typeof slot === 'undefined') {
            this.notifyError(`There is no output slot called ${name}`);
            return null;
        }

        else {
            return slot;
        }
    }

    getInput(name) {
        const slot = this.core.slots.input[name];
        if (typeof slot === 'undefined') {
            this.notifyError(`There is no input slot called ${name}`);
            return null;
        }

        else {
            return slot.getValue();
        }


    }

    prepareOutput(lastOutput) {
        // return same message to all slots
        let results = {}
        const slots = this.core.slots;
        Object.keys(slots.output).forEach(slotName => {
            results[slotName] = lastOutput;
        });

        return results;

        // or a custom response foreach
        // return {
        //     output_slot_name_a: results.value.foo,
        //     output_slot_name_b: results.value.b
        // };
    }

    clearInputs() {
        super.clearInputs();

        Object.values(this.core.slots.input).forEach(slot => {
            slot.clear();
        });
    }
    
    /**
     * check foreach slot their links, and regenerate link or unlink if needed
     */
    updateLinks() {
        // assume that slots instances are already created by @skills/slot-io
        // const slots = this.core.slots;
        
        // Object.values(slots.input).forEach((slot) => {
        //     this.debug(slot);

        //     this.services.link.create({
        //         input_slot_id: slot._id,
        //         input_owner_id: slot.owner_id,
        //         input_slot_name: slot.name,
        //         output_slot_id: this._id,
        //         output_owner_id: this.owner_id,
        //         output_slot_name: this.name
        //     });
        // });

        
        // Object.values(slots.output).forEach((slot) => {
        //     this.debug(slot);
        //     this.services.link.create({
        //         input_slot_id: this._id,
        //         input_owner_id: this.owner_id,
        //         input_slot_name: this.name,
        //         output_slot_id: slot._id,
        //         output_owner_id: slot.owner_id,
        //         output_slot_name: slot.name
        //     });
        // });
    }

    onLive(settings) {
        super.onLive(settings);
        this.updateLinks();
    }

    static parse(document) {
        const slotBox = new SlotBox(document);
        return slotBox;
    }

    createSlot(settings) {
        if (!this.core.slots[settings.type]) {
            this.core.slots[settings.type] = {};
        }

        this.core.slots[settings.type][settings.name] = this.services.slot.create(settings);
    }
}

module.exports = SlotBox;