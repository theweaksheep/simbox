module.exports = {
    name: 'jobber',
    version: '1.0.0',
    
    rules: {
        'input-check': [ 'input-not-null' ],
        'output-check': [ 'output-not-null' ],
        'output-outdated': [ 'output-outdated-resolve-backward' ],
        trigger: [ 'always-trigger' ]
    }
}