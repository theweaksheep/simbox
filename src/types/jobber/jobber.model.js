const Agent = require('../agent');

const configuration = require('./jobber.model.config');

class Jobber extends Agent {
    constructor(settings) {
        settings = Object.assign({}, configuration, settings);
        super(settings);

        this.launchSetupSkills(settings);
    }

    launchSetupSkills(settings) {
        const rules = this.core.rules['setup'];

        if (rules.length === 0) {
            return true;
        }
        
        Array.from(rules).forEach(name => {
            return this.skills[name].initialize(settings);
        });
    }

    iniCoreData() {
        this.core = {
            rules: {
                setup: new Set(),
                "input-check": new Set(),
                "input-sync": new Set(),
                trigger: new Set(),
                "output-check": new Set(),
                "output-outdated": new Set(),
                "output-sending": new Set()
            },

            lastOutput: {
                timestamp: null,
                value: null
            },

            pending_inputs: {},
            lastTrigger: null
        };
    }

    addRule(domain, skillName) {
        if (typeof this.core.rules[domain] !== 'undefined') {
            const skill = this.services.skill.getByName(skillName);

            if (skill === null) {
                this.notifyError(`
                    Unable to add rule ${domain}/${skillName}\n
                   
                `, true);

                return;
            }
 
            this.core.rules[domain].add(skillName);
            skill.attach(this);
        }

        else {
            this.notifyError(`Cannot set rules : ${domain} in Jobber. Candidats are : ${ Object.keys(this.core.rules) }`, true);
        }
    }

    onSetup(settings) {
        super.onSetup(settings);

        this.iniCoreData();
        this.onSetupRules(settings);
        this.onSetupMethodOverride(settings);
    }

    onSetupMethodOverride(settings) {
        if (typeof settings.override === 'undefined') {
            return;
        }

        const overridedMethods = settings.override;
        if (typeof overridedMethods.prepareOutput === 'function') {
            this.prepareOutput = overridedMethods.prepareOutput.bind(this);
        }
    }

    addRulesFromObject(rulesObject) {
        Object.keys(rulesObject).forEach(domain => {
            rulesObject[domain].forEach(ruleName => {
                this.addRule(domain, ruleName);
            });
        });
    }

    onSetupRules(settings) {
        if (typeof settings.setRules === 'undefined') {
            this.addRulesFromObject(configuration.rules)

            if (typeof settings.useRules !== 'undefined') {
                this.addRulesFromObject(settings.useRules);
            }
        }

        else if (typeof settings.setRules !== 'undefined') {
            this.addRule(settings.setRules);
        }
    }
    
    async resolve_rules(domain, message) {
        const rules = this.core.rules[domain];

        if (rules.length <= 0) {
            return true;
        }
        
        const result = (await Promise.all(Array.from(rules).map(async name => {
            this.debug("resolve_rules", domain, name);
            
            let status = false;
            try {
                status = await this.skills[name].check(message);
            } 

            catch(error) {
                this.notifyError(`${domain}-${name}-${error}`);
            }

            finally {
                return status;
            } 
        }))).every(test => test === true);

        // check if all rules checking return true
        return result;
    }

    async onNewMessage(message) {
        let continueProcess = true;
        
        continueProcess = await this.resolve_rules('input-check', message);
        if (continueProcess !== true) {
            return;
        }

        continueProcess = await this.resolve_rules('input-sync', message);
        if (continueProcess !== true) {
            return;
        }

        continueProcess = await this.resolve_rules('trigger', message);
        if (continueProcess !== true) {
            return;
        }

        else {
            this.job(message);
        }
    }

    getTimestamp() {
        return this.skills['message-same-timestamp'].timestamp;
    }

    // updateLastTrigger(message = { 
    //     getTimestamp: () => { return Date.now() }
    // }) {
    //     const timestamp = message.getTimestamp(); 
    //     if (timestamp >= this.core.lastTrigger) {
    //         this.core.lastTrigger = timestamp;
    //     }
    // }

    async job(message = {}) {
        if (typeof message.getTimestamp !== 'function') {
            message.getTimestamp = () => { return Date.now() }
        }

        try {
            const result = await this.onJob(message);

            
            this.clearInputs();
            this.clearPendingMessage();
            
            if (message !== null) {
                this.core.lastOutput.timestamp = message.getTimestamp();
            }

            // something was wrong with message, force correct timestamp
            if (!this.core.lastOutput.timestamp) {
                this.core.lastOutput.timestamp = Date.now();
            }

            this.core.lastOutput.value = result;
            const output = this.prepareOutput(this.core.lastOutput.value);
            
            try { 
                this.debug('output', output);
                if (await this.resolve_rules('output-check', output)) {
                    this.propageResult(output);
                }

                else {
                    this.notifyError('output-check fails with params', output);
                }
            } 

            catch (error) {
                this.notifyError(error);
            }

            
        }

        catch(e) {
            this.notifyError(e);
        }
    }

    clearInputs() {
        this.clearPendingMessage();
    }
    
    clearPendingMessage() {
        const messages = Object.values(this.core.pending_inputs);
        messages.forEach(message => {
            message.delete();
            delete this.core.pending_inputs[message.sender_id];
        });
    }

    prepareOutput(lastOutput) {
        return this.core.lastOutput.value;
    }

    propageResult(results) {
        const rules = this.core.rules['output-sending'];

        if (rules.length === 0) {
            return true;
        }
        
        Array.from(rules).forEach(name => {
            return this.skills[name].send(results);
        });
    }

    async onJob(message) {
        return 8;
    }
}

module.exports = Jobber;