const Entity = require('../entity');
const configuration = require('./link.config');

class Link extends Entity {
    constructor(settings = {}) {
        super(Object.assign({}, configuration, settings));
        this.input_slot_id = settings.input_slot_id; 
        this.input_owner_id = settings.input_owner_id; 
        this.input_slot_name = settings.input_slot_name; 
        this.output_slot_id = settings.output_slot_id; 
        this.output_slot_name = settings.output_slot_name;
        this.output_owner_id = settings.output_owner_id;
    }

    die() {
        const slot_service = this.services.slot;
        const input_slot = slot_service.getById(this.input_slot_id);
        input_slot.onLinkDeleted(this._id);

        const output_slot = slot_service.getById(this.output_slot_id);
        output_slot.onLinkDeleted(this._id);
    }
}

module.exports = Link;