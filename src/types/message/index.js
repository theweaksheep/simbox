const instances = {};
let counter = 0;

// const isObjectEmpty = require('@tools/isObjectEmpty');

class Message {
    constructor(settings = {}) {
        this.content = typeof settings.content !== 'undefined' ? settings.content : '';
        this.timestamp = settings.timestamp || null;
        this._id = (++counter) + Date.now();
        this.receiver_id = settings.receiver_id || null;
        this.sender_id = settings.sender_id || null;

        instances[this._id] = this;
    }

    setReceiver(receiver_id) {
        this.receiver_id = receiver_id;
    }

    setSender(sender_id) {
        this.sender_id = sender_id;
    }

    getSender(sender_id) {
        return this.sender_id;
    }

    forceTimestamp() {
        if (this.timestamp === null) {
            this.timestamp = Date.now();
        }
    }

    hasTimestamp() {
        return Number.isFinite(this.timestamp);
    }

    copy() {
        return new Message(this);
    }

    static fromString(string) {
        return new Message({ content : string });
    }

    static getById(message_id = null) {
        if (
            message_id !== null &&
            typeof instances[message_id] !== 'undefined'
        ) {
            return instances[message_id];
        } 

        else {
            throw new Error('incorrect message_id : ' + message_id);
        }
    }

    delete() {
        delete instances[this._id];
    }

    isEmpty() {
        return (
            typeof this.content === 'undefined'|| 
            this.content === null 
            // ||
            // (typeof this.content === 'string' && this.content.trim() == '') ||
            // isObjectEmpty(this.content)
        );
    }

    setTimestamp(value) {
        this.timestamp = value;
    }

    getTimestamp() {
        return this.timestamp;
    }
}

module.exports = Message;