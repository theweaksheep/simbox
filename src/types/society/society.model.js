const Entity = require('../entity');

class Society extends Entity {
    constructor(settings) {
        super(settings);
        this.status = settings.status || "uncertain";
        this.created_at = settings.created_at || Date.now();
        this.population = settings.population || [];
        this.context = settings.context || {};
    }

    static parse(document) {
        let society = new Society(document);

        society.context = document.context || {};

        society.populationLoaded = false;
        society.lastPopulationSyncWithDB = null;
        society.lastSyncWithDB = Date.now();
        
        return society;
    }

    removeFromPopulation(agent_id) {
        const index = this.population.indexOf(agent_id);
        if (index >= 0) {
            this.population.splice(index, 1);
        }
    }
    
    removeAgent(agent_id) {
        const index = this.population.indexOf(agent_id);
        if (index >= 0) {
            this.population.splice(index, 1);
        }
    }

    addAgent(agent_id) {
        this.population.push(agent_id);
    }

    delete() {
        this.population.forEach((agent_id) => {
            this.services.agent.getById(agent_id).delete();
        });

        this.population.length = 0;
    }

    toString() {
        return JSON.stringify({
            population: this.population,
            _id: this._id,
            created_at: this.created_at,
            name: this.name,
            status: this.status,
            context: this.context,
        });
    }

    getKey() {
        if (this.alreadyInDB) return this._id;
        else return this.name;
    }

    // todo move default parms to society service
    setup() {
        if (!this.context) this.context = {};
        if (!this.type) this.type = 'Society'; 
        this.server_id = Society.counter++;
        this.status = "active";
        this.name = "#Society-" + Society.counter;

        if (this._id === null) {
            this._id = this.server_id;
        }
    }
}

module.exports = Society;