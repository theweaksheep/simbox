# April 2019 #

## 09 - Test and Debug ##

Use simbox module in crypto-bot project reveal some bug with parser service and instantiation of ressources. We fix this point and add a functionnal test in order to prevent regressions.

## 07 - Externalize simbox module ##

The existing prototype of simbox implemented for "trading-bot" app was more complex each days, so we decide externalize all this logic in new module, tadaaam !
