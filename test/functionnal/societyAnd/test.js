'use strict';

const chai = require('chai');
const assert = chai.assert;

const simbox = require('../../../index');

simbox.storeRessource('And', simbox.getRessource('math/And.sb'));
simbox.storeRessource('Herald', simbox.getRessource('tool/Herald.sb'));

const AgentService = simbox.getService('agent')

const fixtures = require('../../fixtures/slotBox/examples');

const test = {
    data: {
        counter: 0
    }
};

describe('Herald', () => {
    it('can create instance without any slot data', () => {
        let agent = null;
        assert.doesNotThrow(() => {
            agent = AgentService.create({
                verboseMode: true,
                type: 'Herald'
            });
        });

        assert.isNotNull(agent);
        assert.exists(agent.core.slots.output);
        assert.exists(agent.core.slots.output.message);
    })
})

describe('@simbox/slot-box/herald-and-society', function() {
    this.timeout(15000);
    this.slow(1000);

    before(() => {
        test.entryA = AgentService.create({
            verboseMode: true,
            ...fixtures.herald_1
        });

        test.entryB = AgentService.create({
            verboseMode: true,
            ...fixtures.herald_2
        });
   
        test.receiver = AgentService.create(fixtures.and_debug);

        test.entryA.core.slots.output.message.link(test.receiver.core.slots.input.valueA);
        test.entryB.core.slots.output.message.link(test.receiver.core.slots.input.valueB);        
        
        // make agents alive
        test.entryA.live();
        test.entryB.live();
        test.receiver.live();
    });

    beforeEach(() => {
        test.data.counter = 0;
        test.data.valueA = null;
        test.data.valueB = null;
    })

    it('slot-rules are respect with almost-sync sb', (done) => {
        test.entryA.store('value', true);
        test.entryB.store('value', false);

        test.receiver.onJob = function() {
            test.data.counter++;
            const valueA = test.receiver.core.slots.input.valueA.getValue();
            const valueB = test.receiver.core.slots.input.valueB.getValue();

            setTimeout(function() {
                assert.strictEqual(test.data.counter, 1, 'there not only one call');
                assert.strictEqual(true, valueA, 'value is not correct');
                assert.strictEqual(false, valueB, 'value is not correct');
                assert.isFalse(valueA && valueB, 'it must be false !!');
            
                done();
            }, 200);

            return valueA && valueB;
        };

        test.entryA.job();
        test.entryB.job();
    });

    it('slot-rules are respect with 200-delayed sb', (done) => {
        test.entryA.store('value', true);
        test.entryB.store('value', true);

        test.receiver.onJob = function() {
            test.data.counter++;
            const valueA = test.receiver.core.slots.input.valueA.getValue();
            const valueB = test.receiver.core.slots.input.valueB.getValue();


            assert.isTrue(valueA);
            assert.isTrue(valueB);

            setTimeout(function() {
                assert.strictEqual(test.data.counter, 2, 'there not only two calls');
                assert.strictEqual(true, valueA);
                assert.strictEqual(true, valueB);
                assert.isTrue(valueA && valueB);
                setTimeout(() => {
                    done();
                }, 50);
            }, Math.random() * 400 + 220);
        

            return valueA + valueB;
        };

        test.entryA.job();
        setTimeout(() => {
            test.entryB.job();
        }, 200);
    });
});
