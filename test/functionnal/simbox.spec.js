const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

const assert = chai.assert;
const simbox = require('../../index');

describe('[functionnal] - simbox-core', () => {
    before(() => {
        // do not log error
        simbox.notifyError = (error) => { throw error}
    })
    it('create unknown ressource throws an error', () => {
        assert.throw(() => simbox.create('unknown-ressource'));
    })

    it('store ressource update parser service', () => {
        const timestamp = Date.now();
        class Foo { constructor() { this._id = timestamp }}

        simbox.storeRessource('foo', Foo);

        const instance = simbox.create('foo');
        assert.isDefined(simbox.services.parser.container.foo);
        assert.isFunction(simbox.services.parser.container.foo, 'function');
        assert.strictEqual(instance._id, timestamp);
    })

    it('can record a new skill', () => {
        assert.isArray(simbox.listSkills());
        const skillsNumber = simbox.listSkills().length;
        class ASkill { constructor() { this._id = timestamp }}
        simbox.services.skills.record('a-skill', ASkill);
        const skills = simbox.listSkills();
        assert.isArray(skills);
        assert.strictEqual(skillsNumber + 1, skills.length);
        assert.isTrue(skills.indexOf('a-skill') >= 0);
    })
    
    it('can retrieve a skill using skill-service', () => {
        const skill = simbox.services.skills.getById('a-skill');
        assert.strictEqual(typeof skill, 'function');
    })

    it('can retrieve a skill using getSkill', () => {
        const skill = simbox.getSkill('a-skill');
        assert.strictEqual(typeof skill, 'function');
    })


    it('getSkill and skill-service return the same result for retrieve', () => {
        const a = simbox.getSkill('a-skill');
        const b = simbox.services.skills.getById('a-skill');
        assert.equal(a, b);
    })

    it('can record a custom service', () => {
        const service_name = 'custom-service-name';
        assert.doesNotThrow(() => {
            const EntityService = simbox.getRessource('entity-service');
            class CustomService extends EntityService {
                constructor(settings) {
                    super(settings);
                }
            }
    
            simbox.recordService('custom', CustomService, { name: service_name });
        });
      
        const service = simbox.getService('custom');
        assert.isNotNull(service);
        assert.strictEqual(service.name, service_name);

        const ressource = simbox.getRessource('custom-service');
        assert.isNotNull(ressource);
    })
});
