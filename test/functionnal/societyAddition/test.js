'use strict';

const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

const assert = chai.assert;
const simbox = require('../../../index');

const RandomInt = simbox.getRessource('math/RandomInt.sb');
const Addition = simbox.getRessource('math/Addition.sb');

simbox.storeRessource('RandomInt', RandomInt);
simbox.storeRessource('Addition', Addition);

const AgentService = simbox.getService('agent')

const test = {
    data: {
        counter: 0
    }
};

describe('[functionnal] @simbox/slot-box/suite_1', function() {
    this.timeout(15000);

    before(() => {
        test.randomA = AgentService.create({
            name: "A",
            status: 'active',
            type: 'RandomInt',
            context: {
                slots: {
                    output: {
                        number: {
                            active: false,
                            name: "number",
                            type: "output",
                            defaultValue: 42
                        }
                    }
                }
            },
            
            verboseMode: true
        });
        
        test.randomA.enableTestMode();

        test.randomB = AgentService.create({
            name: "B",
            status: 'active',
            type: 'RandomInt',
            context: {
                slots: {
                    output: {
                        number: {
                            active: false,
                            name: "number",
                            type: "output",
                            defaultValue: 42
                        }
                    }
                }
            },
            
            verboseMode: true
        });


        test.randomB.enableTestMode();

        test.addition = AgentService.create({
            name: "Addition",
            status: 'active',
            type: 'Addition',
            context: {
                slots: {
                    input: {
                        valueA: {
                            defaultValue: 42
                        },

                        valueB: {
                            defaultValue: 42
                        },


                    }
                }
            },
            
            verboseMode: true
        });


        test.addition.enableTestMode();

        test.randomA.core.slots.output.number.link(test.addition.core.slots.input.valueA);
        test.randomB.core.slots.output.number.link(test.addition.core.slots.input.valueB);        
        
        // make agents alive
        test.randomA.live();
        test.randomB.live();
        test.addition.live();
    });

    beforeEach(() => {
        test.data.counter = 0;
        test.data.valueA = null;
        test.data.valueB = null;
    })

    it('slot-rules are respect with almost-sync sb', (done) => {
        test.randomA.onJob = function() {
            const val = Math.random() * 100;
            test.data.valueA = val;

            return val;
        };

        test.randomB.onJob = function() {
            const val = Math.random() * 100;
            test.data.valueB = val;

            return val;
        };

        test.addition.onJob = function() {
            test.data.counter++;
            const valueA = test.addition.core.slots.input.valueA.getValue();
            const valueB = test.addition.core.slots.input.valueB.getValue();

            setTimeout(function() {
                assert.strictEqual(test.data.counter, 1);
                assert.strictEqual(test.data.valueA, valueA);
                assert.strictEqual(test.data.valueB, valueB);
                done();
            }, 200);

            return valueA + valueB;
        };

        test.randomA.job();
        test.randomB.job();
    });

    it('slot-rules are respect with 200-delayed sb', (done) => {
        test.randomA.onJob = function() {
            const val = Math.ceil(Math.random() * 100);
            test.data.valueA = val;

            return val;
        };

        test.randomB.onJob = function() {
            const val = Math.ceil(Math.random() * 100);
            test.data.valueB = val;

            return val;
        };

        test.addition.onJob = function() {
            test.data.counter++;
            const valueA = test.addition.core.slots.input.valueA.getValue();
            const valueB = test.addition.core.slots.input.valueB.getValue();


            assert.strictEqual(test.data.valueA, valueA);
            assert.strictEqual(test.data.valueB, valueB);

            if (test.data.counter === 2) {
                setTimeout(function() {
                    assert.strictEqual(test.data.counter, 2);
                    done();
                }, 200);
            }
            

            return valueA + valueB;
        };

        test.randomA.job();
        setTimeout(() => {
            test.randomB.job();
        }, 200);
    });
});
