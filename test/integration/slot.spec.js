'use strict';
const chai = require('chai');
const assert = chai.assert;

const simbox = require('../../index');
const data = require('../fixtures/slot/examples');
const sb_data = require('../fixtures/slotBox/examples');
const AsyncTest = require('async-mocha');
const LinkService = simbox.getService('link');

const SlotBox = simbox.getRessource('slot-box');

const instances = {
    test_1: {},
    test_2: {}
};

describe('[integration] @simbox/slot-box/suite_1', () => {
    before(() => {
        instances.test_1 = {
            sb_input: simbox.create('slot-box', {
                name: 'sb_input',
                type: 'SlotBox',
                status: 'active',
                context: {
                    slots: {
                        input: {
                            si_1: data.slot_input_inactive
                        }
                    }
                }
            }),

            sb_output: simbox.create('slot-box', {
                name: 'sb_output',
                type: 'SlotBox',
                status: 'active',
                context: {
                    slots: {
                        output: {
                            so_1: data.slot_output_inactive
                        }
                    }
                }
            })
        };

        const sb_input = instances.test_1.sb_input;
        sb_input.enableTestMode();

        const sb_output = instances.test_1.sb_output;
        sb_output.enableTestMode();
        sb_output.prepareOutput = function() {
            return {
                so_1: sb_output.core.slots.output.so_1.defaultValue
            };
        };
    });

    it('manually linking slot', () => {
        const sb_input = instances.test_1.sb_input;
        const sb_output = instances.test_1.sb_output;

        const slot_in = sb_input.core.slots.input.si_1;
        const slot_out = sb_output.core.slots.output.so_1;  

        const link = slot_out.link(slot_in);
        const link_id = link._id;
        const recorded_link = LinkService.getById(link._id);

        assert.strictEqual(link, recorded_link, 'recorded link and created link are not the same');
        
        assert.isTrue(slot_in.link_id.has(link_id));
        assert.isTrue(slot_out.link_id.has(link_id));
        assert.strictEqual(link.output_owner_id, slot_out.owner_id, 'bad output_owner_id');
        assert.strictEqual(link.input_owner_id, slot_in.owner_id, 'bad input_owner_id');
        assert.strictEqual(slot_in.active, slot_out.active, 'slot are not actives');
        assert.isTrue(slot_in.active, 'slot is not active');

        assert.isTrue(slot_in.checkLinkSanity(), 'bad link sanity for input slot');
        assert.isTrue(slot_out.checkLinkSanity(), 'bad link sanity for output slot');
    });

    it('@skill/forward-output-using-job', async () => {
        const sb_output = instances.test_1.sb_output;
        assert.isFalse(await AsyncTest.getAnError(sb_output.job()));
    });


    it('@skill/allowed-slot-input', (done) => {
        const sb_output = instances.test_1.sb_output;
        const sb_input = instances.test_1.sb_input;

        let boxTriggered = false;

        sb_input.onJob = function() {
            boxTriggered = true;
            return 'osef';
        }

        sb_output.job().then(() => {
            setTimeout(() => {
                assert.isTrue(boxTriggered, "The box was not triggered");
                done();
            }, 20);
        });
    });
    
});


describe('@simbox/slot-box/suite_2', () => {
    before(() => {
        instances.test_2 = {
            sb_input: null,
            sb_output: null
        };
    });

    it('SlotBox::parse (1)', () => {
        assert.doesNotThrow(
            () => { 
                instances.test_2.sb_input = SlotBox.parse({
                    ...sb_data.albert,
                    app: simbox
                });
            }
        );

        const sb_input = instances.test_2.sb_input;
    
        assert.isNotNull(sb_input);
        assert.strictEqual(Object.keys(sb_input.slots.input).length, 1);
        assert.strictEqual(sb_input.slots.input.si_1.owner_id, sb_input._id);   
    });


    it('SlotBox::parse (2)', () => {
        assert.doesNotThrow(
            () => {
                instances.test_2.sb_output = SlotBox.parse({
                    ...sb_data.rupert,
                    app: simbox
                });
            }
        );
        
        const sb_output = instances.test_2.sb_output;
    
        assert.isNotNull(sb_output);
        assert.strictEqual(Object.keys(sb_output.slots.output).length, 1);
        assert.strictEqual(sb_output.slots.output.so_1.owner_id, sb_output._id);   
    });

    //  still needed ?
    // it('live() regenerate links and test their sanity', () => {
    //     const sb_output = instances.test_2.sb_output;
    //     const sb_input = instances.test_2.sb_input;
    //     sb_output.live();
    //     sb_input.live();


    //     const slot_in = sb_input.core.slots.input.si_1;
    //     const slot_out = sb_output.core.slots.output.so_1;  

    //     assert.strictEqual(slot_in.link_id.has(link_id), slot_out.link_id.has(link_id));
    //     assert.strictEqual(link.output_owner_id, slot_out.owner_id);
    //     assert.strictEqual(link.input_owner_id, slot_in.owner_id);
    //     assert.strictEqual(slot_in.active, slot_out.active);
    //     assert.isTrue(slot_in.active);

    //     assert.isTrue(slot_in.checkLinkSanity());
    //     assert.isTrue(slot_out.checkLinkSanity());
    // });
});

describe('@simbox/slot-box/suite_3', () => {
    before(() => {
        instances.test_3 = {
            sb_input_1: simbox.create('slot-box', {
                name: 'sb_input_1',
                type: 'SlotBox',
                status: 'active',
                context: {
                    slots: {
                        input: {
                            si_1: data.slot_input_inactive
                        }
                    }
                }
            }),

            sb_input_2: simbox.create('slot-box', {
                name: 'sb_input_2',
                type: 'SlotBox',
                status: 'active',
                context: {
                    slots: {
                        input: {
                            si_1: data.slot_input_inactive
                        }
                    }
                }
            }),

            sb_output: simbox.create('slot-box', {
                name: 'sb_output',
                type: 'SlotBox',
                status: 'active',
                context: {
                    slots: {
                        output: {
                            so_1: data.slot_output_inactive
                        }
                    }
                }
            })
        };

        const sb_input_1 = instances.test_3.sb_input_1;
        sb_input_1.enableTestMode();
        const sb_input_2 = instances.test_3.sb_input_2;
        sb_input_2.enableTestMode();

        const sb_output = instances.test_3.sb_output;
        sb_output.enableTestMode();
        sb_output.prepareOutput = function() {
            return {
                so_1: sb_output.core.slots.output.so_1.defaultValue
            };
        };
    });

    it('manually linking slot', () => {
        const sb_input_1 = instances.test_3.sb_input_1;
        const sb_input_2 = instances.test_3.sb_input_2;
        const sb_output = instances.test_3.sb_output;

        const slot_in_1 = sb_input_1.core.slots.input.si_1;
        const slot_in_2 = sb_input_2.core.slots.input.si_1;
        const slot_out = sb_output.core.slots.output.so_1;  

        const link_1 = slot_out.link(slot_in_1);
        const link_2 = slot_out.link(slot_in_2);
        const link_id_1 = link_1._id;
        const link_id_2 = link_2._id;
        const recorded_link_1 = LinkService.getById(link_1._id);
        const recorded_link_2 = LinkService.getById(link_2._id);

        assert.strictEqual(link_1, recorded_link_1, 'recorded link and created link are not the same');
        assert.strictEqual(link_2, recorded_link_2, 'recorded link and created link are not the same');
        
        assert.isTrue(slot_in_1.link_id.has(link_id_1));
        assert.isTrue(slot_out.link_id.has(link_id_1));
        assert.strictEqual(link_1.output_owner_id, slot_out.owner_id, 'bad output_owner_id');
        assert.strictEqual(link_1.input_owner_id, slot_in_1.owner_id, 'bad input_owner_id');
        assert.strictEqual(slot_in_1.active, slot_out.active, 'slot are not actives');
        assert.isTrue(slot_in_1.active, 'slot is not active');

        assert.isTrue(slot_in_2.link_id.has(link_id_2));
        assert.isTrue(slot_out.link_id.has(link_id_2));
        assert.strictEqual(link_2.output_owner_id, slot_out.owner_id, 'bad output_owner_id');
        assert.strictEqual(link_2.input_owner_id, slot_in_2.owner_id, 'bad input_owner_id');
        assert.strictEqual(slot_in_2.active, slot_out.active, 'slot are not actives');
        assert.isTrue(slot_in_2.active, 'slot is not active');

        assert.isTrue(slot_in_1.checkLinkSanity(), 'bad link sanity for input slot');
        assert.isTrue(slot_in_2.checkLinkSanity(), 'bad link sanity for input slot');
        assert.isTrue(slot_out.checkLinkSanity(), 'bad link sanity for output slot');
    });

    it('@skill/forward-output-using-job', async () => {
        const sb_output = instances.test_3.sb_output;
        assert.isFalse(await AsyncTest.getAnError(sb_output.job()));
    });


    it('@skill/allowed-slot-input', (done) => {
        const sb_output = instances.test_3.sb_output;
        const sb_input_1 = instances.test_3.sb_input_1;
        const sb_input_2 = instances.test_3.sb_input_2;

        let boxTriggered = false;

        sb_input_1.onJob = function() {
            boxTriggered = true;
            return 'osef_1';
        };

        sb_input_2.onJob = function() {
            boxTriggered = true;
            return 'osef_2';
        };

        sb_output.job().then(() => {
            setTimeout(() => {
                assert.isTrue(boxTriggered, "The box was not triggered");
                done();
            }, 20);
        });
    });
    
});