'use strict';
const chai = require('chai');
const assert = chai.assert;

const simbox = require('../../index');
const sb_data = require('../fixtures/slotBox/basic-society.json');
const SocietyService = simbox.getService('society');
const AgentService = simbox.getService('agent');

simbox.storeRessource('RandomInt', simbox.getRessource('math/RandomInt.sb'));
simbox.storeRessource('Logger', simbox.getRessource('tool/Logger.sb'));

describe('[integration] @services/society', () => {
    before(() => {})

    it('can generate society from json', () => {
        let society = null;
        assert.doesNotThrow(() => {
            try {
                society = SocietyService.fromJSON(sb_data);
            }

            catch(error) {
                this.notifyError(error);
                throw error;
            }
        });
        
        sb_data.agents.forEach(agent => {
            if (agent._id) {
                assert.exists(AgentService.getById(agent._id))
            }
        });

        society.population.forEach(agent_id => {
            assert.exists(AgentService.getById(agent_id))
        });

        assert.strictEqual(society.population.length, sb_data.agents.length, 'JSON-encoded-society and instances have not the same number of agents');
    })
})
