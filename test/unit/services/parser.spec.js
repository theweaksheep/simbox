const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

const assert = chai.assert;
const simbox = require('../../../index');
let parser = null;

describe('[unit] - parser-service', () => {
    it('can create', () => {
        assert.doesNotThrow(() => {
            parser = simbox.getService('parser');
        }, 'Unable to create parser service'); 

        assert.equal(parser.name, 'parser-service');
    })
});
