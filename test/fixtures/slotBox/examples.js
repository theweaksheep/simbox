'use strict';
const slots = require('../slot/examples');

module.exports = {
    albert: {
        _id: '#test_sb_albert',
        name: 'test_sb_albert',
        type: 'SlotBox',
        status: 'active',
        context: {
            slots: {
                input: {
                    si_1: Object.assign({}, slots.slot_input_inactive, {
                        link_id: '#test_sb_rupert-petit_scarabe-#test_sb_albert-stylo_vert',
                        alter_id: '#test_sb_rupert'
                    })
                }
            }
        },
        
        verboseMode: true
    },

    rupert: {
        _id: '#test_sb_rupert',
        name: 'test_sb_rupert',
        type: 'SlotBox',
        status: 'active',
        context: {
            slots: {
                output: {
                    so_1: Object.assign({}, slots.slot_output_inactive, {
                        link_id: '#test_sb_rupert-petit_scarabe-#test_sb_albert-stylo_vert',
                        alter_id: '#test_sb_albert'
                    })
                }
            }
        },
        
        verboseMode: true
    },

    herald_1: {
        _id: '@fixture/herald_1',
        name: 'herald_1',
        type: 'Herald',
        status: 'active',
        context: {
            slots: {
                output: {
                    message: {
                        status: 'inactive'
                    }
                }
            }
        }
    },


    herald_2: {
        _id: '@fixture/herald_2',
        name: 'herald_2',
        type: 'Herald',
        status: 'active',
        context: {
            slots: {
                output: {
                    message: {
                        status: 'inactive'
                    }
                }
            }
        }
    },

    and_debug: {
        _id: '@fixture/and_debug',
        name: 'and_debug',
        type: 'And',
        status: 'active',
        context: {
            slots: {
                input: {
                    valueA: {},
                    valueB: {}
                }
            }
        },
        
        verboseMode: true
    }
}