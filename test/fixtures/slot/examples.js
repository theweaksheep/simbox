module.exports = {
    slot_1: {
        "active": "true",
        "link_id": "888",
        "owner_id": "444",
        "name": "kirikou",
        "type": "input",
        "defaultValue": "42"
    },

    slot_output_inactive: {
        active: false,
        name: "petit_scarabe",
        type: "output",
        defaultValue: "bzzzt"
    },

    slot_input_inactive: {
        active: false,
        name: "stylo_vert",
        type: "input",
        defaultValue: {
            ding: false,
            dong: true
        }
    }
}