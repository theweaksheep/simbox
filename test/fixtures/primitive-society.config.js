module.exports = [
    {
        "inner": [
            "5afff23635f6e814b48b40b8"
        ],
        "outer": [
            "5afff0ac35f6e814b48b40b5"
        ],
        
        "_id": "5afff09e35f6e814b48b40b4",
        "name": "Enzo le triso",
        "created_at": "2018-05-19T09:38:38.467Z",
        "status": "active",
        "context": {
            "algo": "MACDBasic",
            "fast": "2",
            "slow": "4",
            "signal": "2",
            "threshold": "-0.4",
            "thresholdBegin": "-1.2",
            "minValue": "0.5"
        },
        "__v": 2,
        "type": "Worker"
    }, {
        "inner": [
            "5afff23635f6e814b48b40b8"
        ],
        "outer": [
            "5afff0ac35f6e814b48b40b5"
        ],

        "_id": "5afff0ca35f6e814b48b40b7",
        "name": "Roméo le pleo",
        "created_at": "2018-05-19T09:39:22.197Z",
        "status": "active",
        "context": {
            "algo": "MACDBasic",
            "fast": "2",
            "slow": "4",
            "signal": "2",
            "threshold": "-0.4",
            "thresholdBegin": "-1.2",
            "minValue": "-1.2"
        },
        "__v": 2,
        "type": "Worker"
    },
    {
        "inner": [
          "5afff09e35f6e814b48b40b4",
          "5afff0ca35f6e814b48b40b7"
        ],
        "outer": [
          "5afff23635f6e814b48b40b8"
        ],
        "_id": "5afff0ac35f6e814b48b40b5",
        "name": "Alfredo le crado",
        "created_at": "2018-05-19T09:38:52.052Z",
        "status": "active",
        "context": {
          "odeur": "0.5"
        },
        "__v": 3,
        "type": "Grinder"
    },

    {
        "inner": [
        "5afff0ac35f6e814b48b40b5"
        ],
        "outer": [
        "5afff0ca35f6e814b48b40b7",
        "5afff09e35f6e814b48b40b4"
        ],
        "_id": "5afff23635f6e814b48b40b8",
        "name": "di caprio",
        "created_at": "2018-05-19T09:45:26.413Z",
        "status": "active",
        "context": {
        "currency": "XETHZEUR",
        "capital": "1000",
        "fee": "0.2"
        },
        "__v": 3,
        "type": "Trader"
    }
];