const chai = require('chai');
const assert = chai.assert;


describe('[regression] - avoid hell of bad require / loading', () => {
    it('require types/society', () => {
        assert.strictEqual(typeof require('../../../src/types/society'), 'function');
    })
});
